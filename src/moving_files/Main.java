package moving_files;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите путь к каталогу");
        String inputPath=scanner.nextLine();
        System.out.println("Введите путь к конечному каталогу");
        String outputPath=scanner.nextLine();
        moveFile(inputPath,outputPath);
    }

    public static void moveFile(String inputPath,String outputPath){
        try {

            System.out.println("В каталоге " + inputPath + " содержатся:");
            readDirectory(inputPath);
            Files.move(Paths.get(inputPath), Paths.get(outputPath));
            System.out.println("Подождите, идёт копирование!");
            System.out.println("Готово!Каталог " + inputPath + " перемещён.");
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readDirectory(String inputPath){
        try(DirectoryStream<Path> directoryStream=Files.newDirectoryStream(Paths.get(inputPath))) {
            for (Path path : directoryStream){
                System.out.println(path.getFileName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
